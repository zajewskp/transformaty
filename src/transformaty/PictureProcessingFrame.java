package transformaty;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileNotFoundException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PictureProcessingFrame extends JFrame{

	private static final long serialVersionUID = 2630102169791637255L;
	private final int WND_HEIGHT = 400;
	private final int WND_WIDTH = 1000;
	private JPanel wholeForm;
	private JPanel centerPanel;
	private JPanel openPicturePanel;
	private JPanel openPictureFuctionalPanel;
	private JButton openPictureButton;
	private JFileChooser fileChooser;
	private JLabel pathToPictureLabel;
	private JTextField pathToPictureField;
	private JPanel openPictureInnerPanel;
	private JPanel processPicturePanel;
	private JPanel processPictureFuctionalPanel;
	private JButton processPictureButton;
	private JButton cleanAllButtons;
	private JPanel processPictureInnerPanel;
	private BufferedImage loadedImage;
	private BufferedImage imageAfterProcessing;
	private BufferedImage coeffitients;
	private JPanel netPicturePanel;
	private JPanel netPictureLabelPanel;
	private JLabel netPictureLabel;
	private JPanel netPictureInnerPanel;
	private JPanel settingsPanel;
	private LabelTextField blockSizeField;
	private LabelTextField yField;
	private LabelTextField cbField;
	private LabelTextField crField;
	
	public PictureProcessingFrame(String title){
		super(title);
		config();
		createForm();
	}
	
	private void config(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(1000, 400));
		setMinimumSize(new Dimension(WND_WIDTH, WND_HEIGHT));
		getContentPane().setLayout(new BorderLayout());
	}
	
	private void createForm(){
		getContentPane().add(getWholeForm(),BorderLayout.CENTER);
	}

	private JPanel getWholeForm(){
		if(wholeForm == null){
			wholeForm = new JPanel();
			wholeForm.setLayout(new BorderLayout());
			wholeForm.add(getCenterPanel(),BorderLayout.CENTER);
			wholeForm.add(getSettingsPanel(), BorderLayout.SOUTH);
		}
		return wholeForm;
	}
	
	private JPanel getCenterPanel(){
		if(centerPanel == null){
			centerPanel = new JPanel();
			centerPanel.setLayout(new GridLayout(1,3));
			centerPanel.add(getOpenPicturePanel());
			centerPanel.add(getProcessPicturePanel());
			centerPanel.add(getNetPicturePanel());
		}
		return centerPanel;
	}
	
	private JPanel getOpenPicturePanel(){
		if(openPicturePanel == null){
			openPicturePanel = new JPanel();
			openPicturePanel.setLayout(new BorderLayout());
			openPicturePanel.add(getOpenPictureFuctionalPanel(),BorderLayout.NORTH);
			openPicturePanel.setBorder(BorderFactory.createTitledBorder(""));
			openPicturePanel.add(getOpenPictureInnerPanel(), BorderLayout.CENTER);
		}
		return openPicturePanel;
	}
	
	private JPanel getOpenPictureFuctionalPanel(){
		if(openPictureFuctionalPanel == null){
			openPictureFuctionalPanel = new JPanel();
			openPictureFuctionalPanel.setLayout(new FlowLayout());
			openPictureFuctionalPanel.add(getOpenPictureButton());
			openPictureFuctionalPanel.add(getPathToPictureLabel());
			openPictureFuctionalPanel.add(getPathToPictureField());
		}
		return openPictureFuctionalPanel;
	}
	
	private JButton getOpenPictureButton(){
		if(openPictureButton == null){
			openPictureButton = new JButton("Otworz");
			openPictureButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					getFileChooser().showOpenDialog(PictureProcessingFrame.this);
					String absolutPath = getFileChooser().getSelectedFile().getAbsolutePath();
					getPathToPictureField().setText(absolutPath);
					loadedImage = getImage(absolutPath);
					if(loadedImage != null){
						ImageIcon imageIcon = new ImageIcon(loadedImage);
				        JLabel label = new JLabel();
				        label.setIcon(imageIcon);
				        getOpenPictureInnerPanel().removeAll();
				        getOpenPictureInnerPanel().add(label,BorderLayout.CENTER);
				        getOpenPictureInnerPanel().revalidate();
				        getOpenPictureInnerPanel().repaint();
					}
					else{
						try {
							throw new FileNotFoundException();
						} catch (FileNotFoundException e1) {
							e1.printStackTrace();
						}
					}
				}
			});
		}
		return openPictureButton;
	}
	
	private BufferedImage getImage(String path){
		BufferedImage image = null;
	    try{
	          image = ImageIO.read(new File(path));
        }
        catch (Exception e){
          e.printStackTrace();
        }
	    return image;
	}
	
	private JFileChooser getFileChooser(){
		if(fileChooser == null){
			fileChooser = new JFileChooser();
		}
		return fileChooser;
	}
	
	private JLabel getPathToPictureLabel(){
		if(pathToPictureLabel == null){
			pathToPictureLabel = new JLabel("Sciezka do obrazu:");
		}
		return pathToPictureLabel;
	}
	
	private JTextField getPathToPictureField(){
		if(pathToPictureField == null){
			pathToPictureField = new JTextField(20);
			pathToPictureField.setEditable(false);
		}
		return pathToPictureField;
	}
	
	private JPanel getOpenPictureInnerPanel(){
		if(openPictureInnerPanel == null){
			openPictureInnerPanel = new JPanel();
			openPictureInnerPanel.setLayout(new BorderLayout());
			openPictureInnerPanel.setBorder(BorderFactory.createTitledBorder(""));
		}
		return openPictureInnerPanel;
	}
	
	private JPanel getProcessPicturePanel(){
		if(processPicturePanel == null){
			processPicturePanel = new JPanel();
			processPicturePanel.setLayout(new BorderLayout());
			processPicturePanel.add(getProcessPictureFuctionalPanel(),BorderLayout.NORTH);
			processPicturePanel.add(getProcessPictureInnerPanel(), BorderLayout.CENTER);
			processPicturePanel.setBorder(BorderFactory.createTitledBorder(""));
		}
		return processPicturePanel;
	}
	
	private JPanel getProcessPictureFuctionalPanel(){
		if(processPictureFuctionalPanel == null){
			processPictureFuctionalPanel = new JPanel();
			processPictureFuctionalPanel.setLayout(new FlowLayout());
			processPictureFuctionalPanel.add(getProcessPictureButton());
			processPictureFuctionalPanel.add(getCleanAllButton());
		}
		return processPictureFuctionalPanel;
	}
	
	private JButton getProcessPictureButton(){
		if(processPictureButton == null){
			processPictureButton = new JButton("Przetworz");
			processPictureButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					//TODO: dowolna metoda do zmiany obrazu, tu akurat zrobione jest kopiowanie
					DctTransform dct = new DctTransform((int)blockSizeField.getValue(),yField.getValue(),cbField.getValue(),crField.getValue(),loadedImage);
					imageAfterProcessing = dct.getoutputImage();
					coeffitients = dct.getdctBlockVisualization();
					/////
					
					ImageIcon imageIcon = new ImageIcon(imageAfterProcessing);
			        JLabel label = new JLabel();
			        label.setIcon(imageIcon);
			        getProcessPictureInnerPanel().removeAll();
			        getProcessPictureInnerPanel().add(label,BorderLayout.CENTER);
			        getProcessPictureInnerPanel().revalidate();
			        getProcessPictureInnerPanel().repaint();
			        
			        ImageIcon coeffitientIcon = new ImageIcon(coeffitients);
			        JLabel label2 = new JLabel();
			        label2.setIcon(coeffitientIcon);
			        getNetPictureInnerPanel().removeAll();
			        getNetPictureInnerPanel().add(label2,BorderLayout.CENTER);
			        getNetPictureInnerPanel().revalidate();
			        getNetPictureInnerPanel().repaint();
				}
			});
			
		}
		return processPictureButton;
	}
	
	private BufferedImage processImage(BufferedImage imageForProcessing){	
		ColorModel cm = imageForProcessing.getColorModel();
	    boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
	    WritableRaster raster = imageForProcessing.copyData(imageForProcessing.getRaster().createCompatibleWritableRaster());
	    BufferedImage newImage = new BufferedImage(cm, raster, isAlphaPremultiplied, null);
		return newImage;
	}
	
	private JButton getCleanAllButton(){
		if(cleanAllButtons == null){
			cleanAllButtons = new JButton("Wyczysc wszystko");
			cleanAllButtons.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					loadedImage = null;
					imageAfterProcessing = null;
					coeffitients = null;
					getOpenPictureInnerPanel().removeAll();
					getOpenPictureInnerPanel().revalidate();
					getOpenPictureInnerPanel().repaint();
					getProcessPictureInnerPanel().removeAll();
					getProcessPictureInnerPanel().revalidate();
					getProcessPictureInnerPanel().repaint();
					getNetPictureInnerPanel().removeAll();
					getNetPictureInnerPanel().revalidate();
					getNetPictureInnerPanel().repaint();

				}
			});
		}
		return cleanAllButtons;
	}
	
	private JPanel getProcessPictureInnerPanel(){
		if(processPictureInnerPanel == null){
			processPictureInnerPanel = new JPanel();
			processPictureInnerPanel.setLayout(new BorderLayout());
			processPictureInnerPanel.setBorder(BorderFactory.createTitledBorder(""));
		}
		return processPictureInnerPanel;
	}
	
	private JPanel getNetPicturePanel(){
		if(netPicturePanel == null){
			netPicturePanel = new JPanel();
			netPicturePanel.setLayout(new BorderLayout());
			netPicturePanel.setBorder(BorderFactory.createTitledBorder(""));
			netPicturePanel.add(getNetPictureInnerPanel(), BorderLayout.CENTER);
		}
		return netPicturePanel;
	}
	
	private JPanel getNetPictureLabelPanel(){
		if(netPictureLabelPanel == null){
			netPictureLabelPanel = new JPanel();
			netPictureLabelPanel.setLayout(new FlowLayout());
			netPictureLabelPanel.add(getNetPictureLabel());
		}
		return netPictureLabelPanel;
	}
	
	private JLabel getNetPictureLabel(){
		if(netPictureLabel == null){
			netPictureLabel = new JLabel("wizualizacja transformaty DCT");
		}
		return netPictureLabel;
	}
	
	private JPanel getNetPictureInnerPanel(){
		if(netPictureInnerPanel == null){
			netPictureInnerPanel = new JPanel();
			netPictureInnerPanel.setLayout(new BorderLayout());
			netPictureInnerPanel.setBorder(BorderFactory.createTitledBorder(""));
		}
		return netPictureInnerPanel;
	}
	
	private JPanel getSettingsPanel(){
		if(settingsPanel == null){
			settingsPanel = new JPanel();
			settingsPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
			settingsPanel.add(getBlockSizeField());
			settingsPanel.add(getYField());
			settingsPanel.add(getCbField());
			settingsPanel.add(getCrField());
		}
		return settingsPanel;
	}
	
	private LabelTextField getBlockSizeField(){
		if(blockSizeField == null){
			blockSizeField = new LabelTextField("Rozmiar bloku:",5,2.0,32.0);
		}
		return blockSizeField;
	}
	
	private LabelTextField getYField(){
		if(yField == null){
			yField = new LabelTextField("Y:",5,0.0,1.0);
		}
		return yField;
	}
	
	private LabelTextField getCbField(){
		if(cbField == null){
			cbField = new LabelTextField("Cb:",5,0.0,1.0);
		}
		return cbField;
	}
	
	private LabelTextField getCrField(){
		if(crField == null){
			crField = new LabelTextField("Cr:",5,0.0,1.0);
		}
		return crField;
	}
}