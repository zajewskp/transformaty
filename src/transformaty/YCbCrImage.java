package transformaty;

import java.awt.image.BufferedImage;

public class YCbCrImage{
private int width;
private int height;
private int row=1;
private int col=1;
private double Y[];
private double Cb[];
private double Cr[];

YCbCrImage (BufferedImage inputImage, int row, int col){
	this.width = inputImage.getWidth();
	this.height = inputImage.getHeight();
	this.Y = new double[width*height];
	this.Cb = new double[width*height];
	this.Cr = new double[width*height];
	for (int i = 0; i < height; i++) {
	      for (int j = 0; j < width; j++) {
	        int pixel = inputImage.getRGB(j, i);
	        double red = (pixel >> 16) & 0xff;
	        double green = (pixel >> 8) & 0xff;
	        double blue = (pixel) & 0xff;

	        this.Y[i*width +j] = (double) (0.299 * red + 0.587 * green + 0.114 * blue);
	      	this.Cb[i*width +j] =  (double)(0.5- 0.1687 * red - 0.3313 * green + 0.5 * blue);
	      	this.Cr[i*width +j] = (double) (0.5+0.5 * red - 0.4186 * green - 0.0813 * blue);

	      }
	}
	this.row = row;
	this.col = col;

}

YCbCrImage (int width, int height, double[] l, double[] cB, double[] cR, int row, int col){
	this.width = width;
	this.height = height;
	this.Y = l;
	this.Cb = cB;
	this.Cr = cR;
	this.row = row;
	this.col = col;
	
}
@Override
public String toString(){
	String lString = "l = [ ";
    for (int i = 0; i < height; i++) {  
        for (int j = 0; j < width; j++) {  
        	lString += Y[i*width +j] + ", ";        
        }
        lString += "\n";
    }
    lString += " ]\n";
	lString = "cB = [ ";
    for (int i = 0; i < height; i++) {  
        for (int j = 0; j < width; j++) {  
        	lString += Cb[i*width +j] + ", ";        
        }
        lString += "\n";
    }
    lString += " ]\n";
	lString = "cR = [ ";
    for (int i = 0; i < height; i++) {  
        for (int j = 0; j < width; j++) {  
        	lString += Cr[i*width +j] + ", ";        
        }
        lString += "\n";
    }
    lString += " ]\n";
    return lString;
}

public BufferedImage toRgb(){
	
	BufferedImage ycb =  new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
	for (int i = 0; i < height; i++) {
	      for (int j = 0; j < width; j++) {

	    	  int r   =   (int)(this.Y[i*width +j]  + 1.402 * (this.Cr[i*width +j])-0.5);
	    	  int g   =   (int) (this.Y[i*width +j]    - 0.344136 * (this.Cb[i*width +j]-0.5 ) - 0.714136 * (this.Cr[i*width +j])-0.5);
	    	  int b   =   (int) (this.Y[i*width +j]    + 1.772 * (this.Cb[i*width +j])-0.5);
	    	  if(r < 0){
	    		  r = 0;
	    	  }
	    	  if(g < 0){
	    		  g = 0;
	    	  }
	    	  if(b < 0){
	    		  b = 0;
	    	  }
	    	  if(r > 255){
	    		  r = 255;
	    	  }
	    	  if(g > 255){
	    		  g = 255;
	    	  }
	    	  if(b > 255){
	    		  b = 255;
	    	  }

	    	int val = (r<<16)| (g<<8)| b ;
	    	ycb.setRGB(j,i,val);  
	      }
	}
	return ycb;
}

public int getWidth(){
	return this.width;
}

public int getHeight(){
	return this.width;
}
public int getRows() {
	return row;
}


public void setRow(int row) {
	this.row = row;
}


public int getCol() {
	return col;
}


public void setCol(int col) {
	this.col = col;
}

public void setWidth(int width) {
	this.width = width;
}

public double[] getY() {
	return Y;
}

public double[] getCb() {
	return Cb;
}

public double[] getCr() {
	return Cr;
}

public void setY(double[] y) {
	Y = y;
}

public void setCb(double[] cb) {
	Cb = cb;
}

public void setCr(double[] cr) {
	Cr = cr;
}

}

