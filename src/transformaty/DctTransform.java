package transformaty;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ListIterator;

import javax.imageio.ImageIO;
import javax.swing.text.html.HTMLDocument.Iterator;

import net.coobird.thumbnailator.Thumbnails;

public class DctTransform {
private int blockSize = 8;
int cols = 1;
int rows = 1;
private double yQuality = 1;
private double cbQuality = 1;
private double crQuality = 1;
private int Quality;
private BufferedImage  inputImage = null;
private ArrayList<YCbCrImage> bufferedInputBlocks;
private ArrayList<YCbCrImage> coeffitiens;
private BufferedImage coeffitiensVisualizationY;
private BufferedImage outputImage = null;
private BufferedImage dctBlockVisualization;
public double DivisorsLuminance[] = new double[64];
public double DivisorsChrominance[] = new double[64];

private int[] quantizationMatrix = new int[]
		{16,11,10,16,24,40,51,61,
		 12,12,14,19,26,58,60,55,
		 14,13,16,24,40,57,69,56,
		 14,17,22,29,51,87,80,62,
		 18,22,37,56,68,109,103,77,
		 24,35,55,64,81,104,113,92,
		 49,64,78,87,103,121,120,101,
		 72,92,95,98,112,100,103,99};
private int[] quantizationChrominanceMatrix = new int[]
		{17,18,24,47,99,99,99,99,
		 18,21,26,66,99,99,99,99,
		 24,26,56,99,99,99,99,99,
		 47,66,99,99,99,99,99,99,
		 99,99,99,99,99,99,99,99,
		 99,99,99,99,99,99,99,99,
		 99,99,99,99,99,99,99,99,
		 99,99,99,99,99,99,99,99};


public DctTransform(int blockSize,double yQuality,double cbQuality, double crQuality, BufferedImage inputImage){
	this.blockSize = blockSize;
	this.yQuality = yQuality;
	this.cbQuality = cbQuality;
	this.crQuality = crQuality;
	this.inputImage = inputImage;
	double q = 100*yQuality;
	setQuality((int) q);
	createCoefCanvasArray(blockSize);
	this.bufferedInputBlocks = divideIntoBlocks(inputImage,blockSize);
	this.coeffitiens = this.bufferedInputBlocks;

	for(YCbCrImage block : this.coeffitiens){
		block.setY(dctTransform(block.getY(),blockSize,true));
		block.setCb(dctTransform(block.getCb(),blockSize,false));
		block.setCr(dctTransform(block.getCr(),blockSize,false));
		yCbCrZigZag(block,yQuality,cbQuality,crQuality);
	}
	coeffitiensVisualizationY = mergeBlocks(coeffitiens,blockSize,"coef");
	for(YCbCrImage block : this.coeffitiens){
		block.setY(dctTransformReconstruct(block.getY(),blockSize,true));
		block.setCb(dctTransformReconstruct(block.getCb(),blockSize,false));
		block.setCr(dctTransformReconstruct(block.getCr(),blockSize,false));
	}
	outputImage = mergeBlocks(coeffitiens,blockSize,"final");

}

public void setQuality(int quality){

    Quality = quality;
    if (Quality <= 0)
            Quality = 1;
    if (Quality > 100)
            Quality = 100;
    if (Quality < 50)
            Quality = 5000 / Quality;
    else
            Quality = 200 - Quality * 2;
    
}
public void createCoefCanvasArray(int blockSize) {
	double zoom = 2;
	if(blockSize <= 32){
		if(blockSize <=8){
			zoom = 4;
		}
		if (blockSize == 32){
			zoom = 0.5;
		}
	double[] coef  = new double[blockSize*blockSize*4];
    BufferedImage finalImg = new BufferedImage(blockSize*blockSize+blockSize, blockSize*blockSize+blockSize, BufferedImage.TYPE_INT_ARGB);  


	for(int v = 0; v < blockSize; v++) {
		for(int u = 0; u < blockSize; u++) {
			BufferedImage canvas = new BufferedImage(blockSize, blockSize, BufferedImage.TYPE_INT_ARGB);
			WritableRaster raster = (WritableRaster) canvas.getData();

    		int count =0;
			for(int j = 0; j < blockSize; j++) {
				for(int i = 0; i < blockSize; i++) {

					double value = Math.cos((2*i+1) * u * Math.PI / (blockSize * 2)) *
							Math.cos((2*j+1) * v * Math.PI / (blockSize * 2));
							
					coef[count +0] = (127 + 128 * value);
					coef[count +1] =  (127 + 128 * value);
					coef[count +2]  = (127 + 128 * value);
					coef[count +3]= 255;
					count += 4;
				}
				raster.setPixels(0, 0, blockSize, blockSize, coef);
				canvas.setData(raster);
			}
	        finalImg.createGraphics().drawImage(canvas, blockSize * u+u, blockSize * v+v, null);  	       
		}
	}
	int size = (int) (finalImg.getHeight()*zoom);
	try {
		BufferedImage xxl = Thumbnails.of(finalImg).size(size, size).asBufferedImage();
		File coefArrayFile = new File("coef_array.png");   
		ImageIO.write(xxl, "png", coefArrayFile);
		this.dctBlockVisualization = xxl;
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	}
}
public ArrayList<YCbCrImage> divideIntoBlocks(BufferedImage image, int blockSize){
	this.cols = (int) Math.ceil((double)image.getWidth()/(double)blockSize);
	this.rows = (int) Math.ceil((double)image.getHeight()/(double)blockSize);
	System.out.println("cols " + cols + " rows " + rows);
	File dir = new File("splitted");
	dir.mkdir();
	int count = 0;
	ArrayList<YCbCrImage> imgs = new ArrayList<YCbCrImage>();
    for (int x = 0; x < rows; x++) {  
        for (int y = 0; y < cols; y++) {  
        	BufferedImage img = new BufferedImage(blockSize,blockSize,image.getType());
            Graphics2D gr = img.createGraphics();  
            gr.drawImage(image, 0, 0, blockSize, blockSize, blockSize * y, blockSize * x, blockSize * y + blockSize, blockSize * x + blockSize, null);  
            gr.dispose();  
            imgs.add(new YCbCrImage(img,x,y));
            count++;
        }
     }
	return imgs;
}

public BufferedImage mergeBlocks(ArrayList<YCbCrImage> blocks, int blockSize, String filename){
    java.util.Iterator<YCbCrImage> itr = blocks.iterator();

    BufferedImage finalImg = new BufferedImage(blockSize*cols, blockSize*rows, BufferedImage.TYPE_INT_RGB);  

    for (int i = 0; i < rows; i++) {  
        for (int j = 0; j < cols; j++) {  
            finalImg.createGraphics().drawImage(itr.next().toRgb(), blockSize * j, blockSize * i, null);  
        }  
    }  
    System.out.println("Image concatenated.....");  
    try {
		ImageIO.write(finalImg, "jpg", new File(filename +".jpg"));
	} catch (IOException e) {
		e.printStackTrace();
	}  
    return finalImg;
}

public double[] dctTransform(double[] x, int blockSize, boolean luminance){
	double[] coef = new double[blockSize*blockSize];
	double oneoversqrt2 = 1.0 / Math.sqrt(2);
	double cu, cv, sum;

	for(int v = 0; v < blockSize; v++) {
		for(int u = 0; u < blockSize; u++) {
			
			sum = 0.0;
			for(int j = 0; j < blockSize; j++) {
				for(int i = 0; i < blockSize; i++) {

					sum += x[i*blockSize+j] * 
							Math.cos((2.0*i+1.0) * u * Math.PI / (blockSize * 2.0)) *
							Math.cos((2.0*j+1.0) * v * Math.PI / (blockSize * 2.0));
					
				}
			}
			cu = u == 0.0 ? oneoversqrt2 : 1.0;
			cv = v == 0.0 ? oneoversqrt2 : 1.0;
			coef[v*blockSize + u] = (double) ((2.0 /blockSize) * cu * cv * sum);
/*			if(luminance){
				coef[v*blockSize + u] = (int) Math.round(coef[v*blockSize + u] / (double)this.quantizationMatrix[v*blockSize + u]); 
			}else{
				coef[v*blockSize + u] = (int) Math.round(coef[v*blockSize + u] / (double)this.quantizationChrominanceMatrix[v*blockSize + u]);
			}*/
	    	  

		}

	}
	
	return coef;
}


public void yCbCrZigZag(YCbCrImage block, double y, double cb, double cr) {

		block.setY(zigZag(block.getY(), y));
		block.setCb(zigZag(block.getCb(), cb));
		block.setCr(zigZag(block.getCr(), cr));

}

double[] zigZag(double[] coefs, double ratio) {
	int i=1, j=1;
	double[] compressed = coefs;
	double keep = blockSize*blockSize *ratio;
	
	for (int e = 0; e < blockSize*blockSize; e++) {
		
		if (e >= keep) {
			compressed[(i-1)*blockSize +(j-1)] = 0;
		}
		
		if ((i + j) % 2 == 0) {
			// Even stripes
			if (j < blockSize) j ++;
			else       i += 2;
			if (i > 1) i --;
		} else {
			// Odd stripes
			if (i < blockSize) i ++;
			else       j += 2;
			if (j > 1) j --;
		}

	}
	
	return compressed;
}

public double[] dctTransformReconstruct(double[] x, int blockSize, boolean luminance){
	double[] coef = new double[blockSize*blockSize];
	double oneoversqrt2 = 1.0 / Math.sqrt(2);
	double cu, cv, sum;
	
	for(int i= 0; i < blockSize; i++) {
		for(int j = 0; j < blockSize; j++) {
			
			sum = 0.0;
			for(int v = 0; v < blockSize; v++) {
				for(int u = 0; u < blockSize; u++) {
					
					cu = u == 0.0 ? oneoversqrt2 : 1.0;
					cv = v == 0.0 ? oneoversqrt2 : 1.0;
					
					sum += x[v*blockSize+u] * cu*cv*
							Math.cos((2.0*i+1.0) * u * Math.PI / (blockSize * 2.0)) *
							Math.cos((2.0*j+1.0) * v * Math.PI / (blockSize * 2.0));
					
				}
			}
/*			if(luminance){
				coef[i*blockSize + j] = (int)Math.round(coef[i*blockSize + j] * (double)this.quantizationMatrix[i*blockSize + j]); 
			}else{
				coef[i*blockSize + j] = (int) Math.round(coef[i*blockSize + j] * (double)this.quantizationChrominanceMatrix[i*blockSize + j]);
			}*/
			coef[i*blockSize + j] = (double) ((2.0 /blockSize)  * sum);

		}
	}
	
	return coef;
}

public BufferedImage getoutputImage(){
	return this.outputImage;
}
public BufferedImage getCoeffitients(){
	return this.coeffitiensVisualizationY;
}
public BufferedImage getdctBlockVisualization(){
	return this.dctBlockVisualization;
}
}