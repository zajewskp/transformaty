package transformaty;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class LabelTextField extends JPanel{

	private static final long serialVersionUID = 5300716594622156044L;
	private String labelTxt;
	private int columns;
	private JLabel label;
	private JTextField textField;
	private double minValue;
	private double maxValue;
	private double value;
	
	public LabelTextField(String textForLabel, int columnsForField, double minValueInField, double maxValueInField){
		labelTxt = textForLabel;
		columns = columnsForField;
		minValue = minValueInField;
		maxValue = maxValueInField;
		setLayout(new FlowLayout(FlowLayout.CENTER));
		add(getLabel());
		add(getTextField());
	}
	
	private JTextField getTextField(){
		if(textField == null){
			textField = new JTextField(columns);
			textField.setBackground(Color.GRAY);
			textField.getDocument().addDocumentListener(new DocumentListener() {
				
				@Override
				public void removeUpdate(DocumentEvent e) {
					checkField();
				}
				
				@Override
				public void insertUpdate(DocumentEvent e) {
					checkField();
				}
				
				@Override
				public void changedUpdate(DocumentEvent e) {
					checkField();
				}
			});
		}
		return textField; 
	}
	
	private void checkField(){
		String textInField = textField.getText();
		if(textInField.isEmpty()){
			textField.setBackground(Color.GRAY);
		}
		else{
			boolean wasProblemWithParsing = false;
			try{
				value = Double.parseDouble(textInField);
			}
			catch(Exception e){
				wasProblemWithParsing = true;
			}
			if(wasProblemWithParsing){
				textField.setBackground(Color.RED);
			}
			else{
				if(minValue <= value && value <= maxValue){
					textField.setBackground(Color.GRAY);
				}
				else{
					textField.setBackground(Color.RED);
				}
			}
		}
	}
	
	
	
	private JLabel getLabel(){
		if(label == null){
			label = new JLabel(labelTxt);
		}
		return label;
	}
	
	public void setTextInField(String text){
		textField.setText(text);
	}
	
	public String getTextFromField(){
		return textField.getText();
	}
	public double getValue(){
		return this.value;
	}
}
