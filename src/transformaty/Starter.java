package transformaty;

import javax.swing.SwingUtilities;

public class Starter {

	private static PictureProcessingFrame pictureProcessingWindow;
	private static String title = "Przetwarzanie obrazow";
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	createGUI();
		    }
		});
	}
	
	private static void createGUI(){
		if(pictureProcessingWindow == null){
			pictureProcessingWindow = new PictureProcessingFrame(title);
			pictureProcessingWindow.pack();
			pictureProcessingWindow.setVisible(true);
		}
	}

}